FROM registry.gitlab.com/xrow-public/docker-php-centos7:base-fedora

USER 0

ENV COMPOSER_ALLOW_XDEBUG=0 \
    COMPOSER_MEMORY_LIMIT=-1 \
    COMPOSER_VERSION=2.2.6 \
    SHORT_OPEN_TAG=Off \
    COOKIE_LIFETIME=8640000 \
    OPCACHE_MEMORY_CONSUMPTION=1024 \
    PHP_MEMORY_LIMIT=2048M \
    OPCACHE_REVALIDATE_FREQ=100000 \
    MYSQLI_ALLOW_PERSISTENT=1 \
    LANG=en_US.UTF-8

# skywalking
#RUN yum search grpc && \
#    yum install -y --setopt=tsflags=nodocs php-pear php-devel grpc && \
#    pecl install grpc && \
#    pecl install skywalking --with-grpc= /usr/lib64/php/modules

RUN dnf install -y yum-utils && \
    dnf install -y bind-utils && \
    dnf -y install https://rpms.remirepo.net/fedora/remi-release-34.rpm && \
    dnf config-manager --set-enabled remi && \
    dnf module -y install php:remi-7.4

RUN yum install -y --setopt=tsflags=nodocs optipng --nogpgcheck && \
    yum install -y --setopt=tsflags=nodocs jpegoptim --nogpgcheck && \
    yum install -y --setopt=tsflags=nodocs pngquant --nogpgcheck && \
    yum install -y --setopt=tsflags=nodocs ghostscript --nogpgcheck && \
    yum install -y --setopt=tsflags=nodocs curl --nogpgcheck && \
    yum install -y --setopt=tsflags=nodocs openssh-clients --nogpgcheck  && \
    yum install -y --setopt=tsflags=nodocs jq --nogpgcheck && \
    INSTALL_PKGS="php74-php-pecl-amqp \
                  php74-php-sodium \
                  php74-php-pecl-imagick \
                  php74-php-pecl-redis5 \ 
                  php74-php-pecl-http \
                  php74-php-pecl-msgpack \
                  php74-php-pecl-memcached \
                  php74-php-pecl-lzf \
                  php74-php-pecl-igbinary \ 
                  php74-php-pecl-xdebug3 \
                  php74-php-pecl-zip" && \
    yum install -y --setopt=tsflags=nodocs $INSTALL_PKGS --nogpgcheck && \
    mv /opt/remi/php74/root/usr/lib64/php/modules/* /usr/lib64/php/modules/ && \
    mv /etc/opt/remi/php74/php.d/* /etc/php.d/ && \
    dnf clean all && \
    rm -rf /var/cache/yum

# Copy the S2I scripts from the specific language image to $STI_SCRIPTS_PATH
COPY ./s2i/bin/ $STI_SCRIPTS_PATH

# Copy extra files to the image.
COPY ./root/ /

RUN chmod 755 /usr/libexec/s2i/assemble && \
    chmod 755 /usr/libexec/s2i/run && \
    sed -i "s/memory_limit.*=.*/memory_limit = $PHP_MEMORY_LIMIT/g" $PHP_SYSCONF_PATH/php.ini && \
    sed -i -e 's/track_errors =.*/track_errors = OFF/g' $PHP_SYSCONF_PATH/php.ini

# Add Composer
RUN curl -L https://raw.githubusercontent.com/composer/getcomposer.org/master/web/installer -s | php -- --quiet --install-dir=/usr/bin --filename=composer --version=$COMPOSER_VERSION && \
    rm -Rf /opt/app-root/src/.composer

# @TODO no more ffmpeag, we need to remove it to a webservice
# Add ffmpeg
# RUN dnf -y install https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm && \
#    dnf -y install https://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm && \
#    yum install -y ffmpeg

# Add Composer
# @todo mave /opt/app-root/src a variable or, better, use $CWD
RUN curl -L https://raw.githubusercontent.com/composer/getcomposer.org/master/web/installer -s | php -- --quiet --install-dir=/usr/bin --filename=composer --version=$COMPOSER_VERSION && \
    rm -Rf /opt/app-root/src/.composer

# Not needed now, save space
# Add platform.sh
# RUN curl -L https://github.com/platformsh/platformsh-cli/releases/download/v3.38.1/platform.phar > /usr/bin/platform && \
#    chmod 755 /usr/bin/platform

# Add yarn
RUN curl --silent --location https://dl.yarnpkg.com/rpm/yarn.repo | tee /etc/yum.repos.d/yarn.repo && \
    yum install -y yarn

# Add Symfony
RUN  wget https://get.symfony.com/cli/installer -O - | bash && \
     mv ~/.symfony/bin/symfony /usr/bin/symfony && \
     chmod 755 /usr/bin/symfony

# RUN wget -O phive.phar https://phar.io/releases/phive.phar \
#   && chmod +x phive.phar \
#   && mv phive.phar /usr/bin/phive

# RUN curl -L https://github.com/phpstan/phpstan/releases/download/0.12.48/phpstan.phar -o /usr/bin/phpstan && \
#    chmod a+x /usr/bin/phpstan

RUN rm -Rf .config .pki .rnd .symfony && \
    chmod 600 /etc/ssh/ssh_config.d/50-redhat.conf && \
    chown 1001 /etc/ssh/ssh_config.d/50-redhat.conf

USER 1001
