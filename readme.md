# Versions 

| Version | Type |
| ------ | ------ |
| > 7.4.0 | RHEL8 UBI build und pushed by hand or fedora build by CI/CD |
| <= 7.3.0 | CentOS7 build by CI |


Build RHEL8

podman build -t registry.gitlab.com/xrow-public/docker-php-centos7:base-rhel8 -f Dockerfile.RHEL8 .
podman push registry.gitlab.com/xrow-public/docker-php-centos7:base-rhel8