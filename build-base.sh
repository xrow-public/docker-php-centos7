#!/bin/bash

set -e

while getopts t: flag
do
    case "${flag}" in
        t) TARGET=${OPTARG};;
    esac
done

yum install -y podman-docker go make
go get github.com/cpuguy83/go-md2man
export PATH=$PATH:$(go env GOPATH)/bin

rm -Rf s2i-php-container
git clone --recursive https://github.com/xrowgmbh/s2i-php-container.git
cd s2i-php-container
echo "Start Build"
make build --trace -k -C $PWD TARGET=$TARGET VERSIONS=7.4;
echo "End Build"
DIGEST=$(podman images | awk '{print $3}' | awk 'NR==2')
echo "Push Build registry.gitlab.com/xrow-public/docker-php-centos7:base-$TARGET"
cat <<CONFIG > Dockerfile 
FROM ${DIGEST}
ENV TZ=Europe/Berlin
CMD /usr/libexec/s2i/run
CONFIG
podman build --squash-all --rm --no-cache -t small .
podman login -u $CI_REGISTRY_USER -p $CI_JOB_TOKEN $CI_REGISTRY
podman tag small registry.gitlab.com/xrow-public/docker-php-centos7:base-$TARGET
podman push registry.gitlab.com/xrow-public/docker-php-centos7:base-$TARGET
cd ..
rm -Rf s2i-php-container
echo "Done Build"
